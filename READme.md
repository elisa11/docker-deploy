# Docker deploy repository

## Docker images

### Oracle client

Base image pre Elisa-api

- **OS** - CentOS Linux release 8.3.2011
- **Oracle DB client** - 18.5.0.0.0-3.x86_64

## CI/CD process

![Sequence diagram](sequence.png)
